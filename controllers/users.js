import jwt from 'jsonwebtoken';
// bcrypt used for hashing passwords
import bcrypt from 'bcryptjs';

import User from '../models/user.js';

export const signin = async (req, res) => {
    // destructure email / password from formData in post request from front-end
    const { email, password } = req.body;
    try {
        const existingEmail = await User.findOne({email});
        const existingUsername = await User.findOne({username:email})
        if(!existingEmail && !existingUsername){
            res.send({message: "Email or username does not exist!"})
            return res.status(404).json({message: "Email or username does not exist!"})
        }
        // compare the hashed passwords
        let isPasswordCorrect 
        if(existingUsername){
            isPasswordCorrect = await bcrypt.compare(password, existingUsername.password);
        } else {
            isPasswordCorrect = await bcrypt.compare(password, existingEmail.password);
        }
        if(!isPasswordCorrect) {
            res.send({message: "Wrong password!"})
            return res.status(400).json({message: "Invalid password!"});
        }

        let token;
        if(existingUsername){
            token = jwt.sign({ email: existingUsername.email, id: existingUsername._id }, 'test', {expiresIn: "1h"});
            res.status(200).json({result: existingUsername, token});
        } else {
            token = jwt.sign({ email: existingEmail.email, id: existingEmail._id }, 'test', {expiresIn: "1h"});
            res.status(200).json({result: existingEmail, token});
        }
    } catch (error) {
        res.status(500).json({message: "Something went wrong."})
    }
}

export const signup = async (req, res) => {
    const { email, password, confirmPassword, firstName, lastName, username} = req.body;

    try {
        //check if user already exists
        const existingUser = await User.findOne({email});
        if(existingUser){
            res.send({message: "User email already exists!"})
            return res.status(400).json({message: "User already exists!"})
        }

        const existingUsername = await User.findOne({username});
        if(existingUsername){
            res.send({message: "Username already exists!"})
            return res.status(400).json({message: "Username already exists!"})
        }

        if(password !== confirmPassword) {
            res.send({message: "Passwords do not match!"})
            return res.status(400).json({message: "Passwords don't match!"})
        }
        
        const hashedPassword = await bcrypt.hash(password, 12);
        const result = await User.create({email, password: hashedPassword, name: `${firstName} ${lastName}`, username});
        const token = jwt.sign({ email: result.email, id: result._id }, 'test', {expiresIn: "1h"});

        res.status(200).json({result: result, token});

    } catch (error) {
        res.status(500).json({message: "Something went wrong."})

    }
}
