import jwt from "jsonwebtoken";

const secret = 'test';
// everytime the signed in user performs an action, check if the token is valid
// e.g. user clicks the like button =>
// make sure that user is authenticated => 
// user is valid? auth middleware(NEXT) => 
// like controller
const auth = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];

        // check if token is our own or Google's || if token length is greater than 500, it is Google Auth
        const isCustomAuth = token.length < 500;

        let decodedData;

        if(token && isCustomAuth){
            // verify() will give us the data from each specific token. e.g. username, id
            decodedData = jwt.verify(token, secret)

            req.userId = decodedData?.id;
        } else {
            // decode google auth's token
            decodedData = jwt.decode(token);

            // sub is Google's specific 'Id' that differentiates every single user
            req.userId = decodedData?.sub;
        }
        
        next();
    } catch (error) {
    console.log(error)        
    }
}

export default auth;