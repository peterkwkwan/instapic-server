# InstaPic Server by pkwan

### Getting started

`git clone https://gitlab.com/peterkwkwan/instapic-server.git` from an empty directory.

Git source can be found here: https://gitlab.com/peterkwkwan/instapic-server

### `npm i`
>Installs the necessary dependencies

### `npm run start` 
 >Runs the server in the development mode.
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

### About the API
`/posts`
> Post routes - for CRUD operations for posts on InstaPic

`/user`
> User routes - for log in / signup authentication and username/password checking

### Tech
InstaPic Server makes use of a number of tools, including:

- [bcryptjs] - secure password hashing
- [express] - NodeJS framework
- [jsonwebtoken] - for user authentication
- [mongoose] - for handling actions to and from our MongoDB database
- [MongoDB] - our document-oriented noSQL database
- [Node.js] - back-end Javascript run-time environment


### Server deployed on Heroku
[https://instapic-server-pkwan.herokuapp.com/](https://instapic-server-pkwan.herokuapp.com/)
